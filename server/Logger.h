#ifndef LOGGER_H
#define LOGGER_H
#include <string>
class Logger
{
	public:
		Logger();
		~Logger();
		void log(const std::string &s_);
		void log(const char* s_);
		void log(const std::string &s_, int r);
		void log(const char* s_, int r);

	private:
	
};
#endif