#ifndef UICOMMANDSERVER_H
#define UICOMMANDSERVER_H

#include <string>
class ServerTcp;
class UiCommandServer
{
public:
	enum class Commands { NONE, CLOSE_APP, KILL_COMMAND, GET_APPS };
	void parseInput(const std::string& s);
	void processCmd(ServerTcp& srv);
private:
	Commands cmd_;
	std::string param_;
};

#endif // !UICOMMAND_H

