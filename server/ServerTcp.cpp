#include "ServerTcp.h"
#include <iostream>

#if defined( _WIN32) || defined(__CYGWIN__) 
#include "server_win_specific.h"
using namespace WinServer;
#else
#include "server_lin_specific.h"
using namespace LinServer;
#endif

ServerTcp::ServerTcp(std::string ServerPort) : serverPort_(ServerPort), run_(true)
{
	received_.clear();
	if (initServerFunctions(serverPort_.c_str()) != SERVER_OK) 
	{
		run_ = false;
	};
}

ServerTcp::~ServerTcp()
{
	shutDownServer();
	run_ = false;
};

void ServerTcp::listen()
{	
	received_.clear();
	std::cout << "listening on port " << serverPort_ << "\n";
	if (run_ && (listenForClient() != SERVER_OK) )
	{
		run_ = false;
	};
};

void ServerTcp::receive()
{
	if (run_ && (receiveFromClient(received_) != SERVER_OK) )
	{
		run_ = false;
	};
};

void ServerTcp::send()
{
	if (run_ && (sendToClient(sended_.c_str(), sended_.length()) != SERVER_OK) )
	{
		run_ = false;
	};		
}

std::string ServerTcp::getReceivedMsg()
{
	return received_;
}

void ServerTcp::setSendMsg(std::string send)
{
	sended_ = send;
}

std::string ServerTcp::getSendMsg()
{
	return sended_;
}

bool ServerTcp::isRunning()
{
	return run_;
}

void ServerTcp::stopServer()
{
	run_ = false;
}