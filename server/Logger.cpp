#include "Logger.h"
#include <string>
#include <iostream>

Logger::Logger()
{
	
}

Logger::~Logger()
{
	
}

void Logger::log(const std::string &s)
{
	std::cerr << s << "\n";
}

void Logger::log(const char *s)
{
	std::cerr << s << "\n";
}

void Logger::log(const std::string &s, int r)
{
	std::cerr << s << " "<< r << "\n";
}

void Logger::log(const char* s, int r)
{
	std::cerr << s << " "<< r << "\n";
}
