#include "system_work_lin.h"
#if defined (_WIN32) || defined (__CYGWIN__)
#else

#include "sstream"
#include <sys/types.h>
#include <signal.h>

#include <stdio.h>
#include <iostream>
#include <memory>
#include <string>
#include <array>

namespace LinServer
{

std::string exec()
{
    std::string cmd = "ps -e --format pid,cmd";
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe) 
	{
        return "getting apps failed (popen problem) \n";
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) 
	{
        result += buffer.data();
    }
    return result;
}

    std::stringstream PrintPocesses()
    {
	std::stringstream ss;
	ss << exec() <<"\n";
	return ss;
    }

    bool killApp(std::string PID) 
    {
	    int pid = atoi (PID.c_str());
		if( pid > 0)
		{
            if (kill(pid, SIGKILL) == 0)
				return true;
		}else
		{
			return false;
		};
    }

}
#endif // _WIN32
