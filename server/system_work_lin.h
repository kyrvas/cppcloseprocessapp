#ifndef SYSTEM_WORK_WIN_H
#define SYSTEM_WORK_WIN_H

#if defined (_WIN32) || defined (__CYGWIN__)
#else

#include <sstream>
#include <string>
namespace LinServer
{
	std::string exec();
	std::stringstream PrintPocesses();
	bool killApp(std::string PID);
}
#endif
#endif