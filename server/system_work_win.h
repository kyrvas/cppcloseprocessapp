#ifndef SYSTEM_WORK_WIN_H
#define SYSTEM_WORK_WIN_H
#if defined( _WIN32) || defined(__CYGWIN__)

#include <sstream>
#include <string>
namespace WinServer
{
	std::stringstream PrintProcessNameAndID(int processID);
	std::stringstream PrintPocesses();
	bool killApp(std::string PID);
}
#endif
#endif