#if defined(_WIN32) || defined(__CYGWIN__)

#undef UNICODE
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 512

#include "server_win_specific.h"
#include "Logger.h"

namespace WinServer
{
	Logger logger;
    WSADATA wsaData;
    int iResult;

    SOCKET ListenSocket = INVALID_SOCKET;
    SOCKET ClientSocket = INVALID_SOCKET;

    struct addrinfo* result = NULL;
    struct addrinfo hints;

    int iSendResult;
    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;

    int initServerFunctions(std::string ServerPort) 
    {
        iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (iResult != 0) 
		{
			logger.log("WSAStartup failed with error:", iResult );        
			//printf("WSAStartup failed with error: %d\n", iResult);
            return SERVER_ERROR;
        }

        ZeroMemory(&hints, sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_flags = AI_PASSIVE;

        iResult = getaddrinfo(NULL, ServerPort.c_str(), &hints, &result);
        if (iResult != 0) {
            //printf("getaddrinfo failed with error: %d\n", iResult);
			logger.log("getaddrinfo failed with error:", iResult );        
            WSACleanup();
            return SERVER_ERROR;
        }

        ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
        if (ListenSocket == INVALID_SOCKET) {
//            printf("socket failed with error: %ld\n", WSAGetLastError());
			logger.log("socket failed with error:", WSAGetLastError());        
            freeaddrinfo(result);
            WSACleanup();
            return SERVER_ERROR;
        }

        iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
//            printf("bind failed with error: %d\n", WSAGetLastError());
			logger.log("bind failed with error:", WSAGetLastError());        
            freeaddrinfo(result);
            closesocket(ListenSocket);
            WSACleanup();
            return SERVER_ERROR;
        }

        freeaddrinfo(result);
		
        iResult = listen(ListenSocket, SOMAXCONN);
        if (iResult == SOCKET_ERROR) 
		{
//            printf("listen failed with error: %d\n", WSAGetLastError());
			logger.log("listen failed with error:", WSAGetLastError());        
            closesocket(ListenSocket);
            WSACleanup();
            return SERVER_ERROR;
        }		
        return SERVER_OK;
    }

    int listenForClient()
    {
        ClientSocket = accept(ListenSocket, NULL, NULL);
        if (ClientSocket == INVALID_SOCKET) {
//            printf("accept failed with error: %d\n", WSAGetLastError());
			logger.log("accept failed with error:", WSAGetLastError());        
            closesocket(ListenSocket);
            WSACleanup();
            return SERVER_ERROR;
        }
        return SERVER_OK;
    }

    int sendToClient(const char* sendbuf, int len) 
    {
        iSendResult = send(ClientSocket, sendbuf, len, 0);
        if (iSendResult == SOCKET_ERROR) {
//            printf("send failed with error: %d\n", WSAGetLastError());
			logger.log("send failed with error:", WSAGetLastError());        
            closesocket(ClientSocket);
            WSACleanup();
            return SERVER_ERROR;
        }
        shutdown(ClientSocket, SD_SEND);
//        printf("Bytes sent: %d\n", iSendResult);
		logger.log("Bytes sent:", iSendResult);        
		closesocket(ClientSocket);
        return SERVER_OK;
    }

    int receiveFromClient(std::string& s) 
    {
        do {
                iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
                if (iResult > 0) 
                {
                    s.insert(s.size(), recvbuf, iResult);
//                    printf("Bytes received: %d\n", iResult);
					logger.log("Bytes received:", iResult);        
                }
                else if (iResult == 0)
//                    printf("Connection closing...\n");
					logger.log("Connection closing...");        
                else 
                {
//                    printf("recv failed with error: %d\n", WSAGetLastError());
					logger.log("recv failed with error:", WSAGetLastError());        
                    closesocket(ClientSocket);
                    WSACleanup();
                    return SERVER_ERROR;
                }
        } while (iResult > 0);
        return SERVER_OK;
    }

    int shutDownServer() 
    { 
        closesocket(ListenSocket);
        iResult = shutdown(ClientSocket, SD_BOTH);
        if (iResult == SOCKET_ERROR) {
//            printf("shutdown failed with error: %d\n", WSAGetLastError());
			logger.log("shutdown failed with error:", WSAGetLastError());        
            closesocket(ClientSocket);
            WSACleanup();
            return SERVER_ERROR;
        }
        closesocket(ClientSocket);
        WSACleanup();

        return SERVER_OK;
    }
}
#endif