#include "system_work_win.h"

#if defined( _WIN32) || defined(__CYGWIN__)

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>
#pragma comment(lib, "Psapi.lib")
#define UNICODE

namespace WinServer
{
    std::stringstream PrintProcessNameAndID(int processID)
    {
        std::string ret = "";
        TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
        HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
            PROCESS_VM_READ,
            FALSE, processID);
        if (NULL != hProcess)
        {
            HMODULE hMod;
            DWORD cbNeeded;
            if (EnumProcessModules(hProcess, &hMod, sizeof(hMod),
                &cbNeeded))
            {
                GetModuleBaseName(hProcess, hMod, szProcessName,
                    sizeof(szProcessName) / sizeof(TCHAR));
            }
        }
        std::stringstream os;
        os << szProcessName << " PID " << processID;
        CloseHandle(hProcess);
        return os;
    }

    std::stringstream PrintPocesses()
    {
        const std::string procErr = "couldn't enumarate processes \n";
        DWORD aProcesses[1024], cbNeeded, cProcesses;
        unsigned int i;
        std::stringstream os;
        if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
        {
            os << procErr;
            return os;
        }

        cProcesses = cbNeeded / sizeof(DWORD);
        for (i = 0; i < cProcesses; i++)
        {
            if (aProcesses[i] != 0)
            {
                os << PrintProcessNameAndID(aProcesses[i]).rdbuf() << '\n';
            }
        }
        return os;
    }

    bool killApp(std::string PID) 
    {
        PID = "taskkill /F /T /PID " + PID;
        int ret = ::system(_T( PID.c_str()));
        if (ret == 0)
            return true;
        else
            return false;
    }

}
#endif // _WIN32
