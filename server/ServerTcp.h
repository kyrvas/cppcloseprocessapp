#ifndef SERVERTCP_H
#define SERVERTCP_H

#include <string>
class ServerTcp {
public:
	ServerTcp(std::string ServerPort);
	std::string getReceivedMsg();
	std::string getSendMsg();
	void setSendMsg(std::string send);
	void listen();
	void receive();
	void send();
	~ServerTcp();
	bool isRunning();
	void stopServer();
private:
	bool run_;
	std::string serverPort_;
	std::string received_;
	std::string sended_;
};

#endif // !CLIENTC_H

