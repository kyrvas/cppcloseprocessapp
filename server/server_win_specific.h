#ifndef SERVER_WIN_SPECIFIC_H 
#define SERVER_WIN_SPECIFIC_H

#if (defined (_WIN32) || defined (__CYGWIN__))
#include <string>

#define SERVER_OK 0
#define SERVER_ERROR 1

namespace WinServer
{
	int initServerFunctions(std::string ServerPort);
	int listenForClient();
	int sendToClient(const char* sendbuf, int len);
	int receiveFromClient(std::string& s);
	int shutDownServer();
}
#endif
#endif
