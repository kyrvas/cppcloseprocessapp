#ifndef SERVER_LIN_SPECIFIC_H 
#define SERVER_LIN_SPECIFIC_H

#if defined (_WIN32) || defined (__CYGWIN__)
#else
#include <string>

#define SERVER_OK 0
#define SERVER_ERROR 1

namespace LinServer
{
	int initServerFunctions(std::string ServerPort);
	int listenForClient();
	int sendToClient(const char* sendbuf, int len);
	int receiveFromClient(std::string& s);
	int shutDownServer();
}
#endif
#endif
