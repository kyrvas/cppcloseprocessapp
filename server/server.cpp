#include "server_win_specific.h"
#include <iostream>
#include <string>
#include "ServerTcp.h"
#include "UiCommandServer.h"

int main(int argc, char** argv)
{
	std::string ServerPort = "27015";
	if (argc >= 2)
	{
		ServerPort.assign(argv[1]);
	}
	std::cout << "use: server [ServerPort] ##now ServerPort = " << ServerPort << "\n";
	ServerTcp server(ServerPort);
	UiCommandServer cmd;
		while (server.isRunning()) 
		{
			server.listen();
			server.receive();
			cmd.parseInput(server.getReceivedMsg());
			cmd.processCmd(server);
		}
   return 0;
}
