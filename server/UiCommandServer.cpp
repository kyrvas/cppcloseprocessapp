
#include "UiCommandServer.h"
#include "ServerTcp.h"
#include <iostream>

#define SINGLE_COMMAND_SIZE 1

#if defined( _WIN32) || defined(__CYGWIN__)
#include "system_work_win.h"
using namespace WinServer;
#else
#include "system_work_lin.h"
using namespace LinServer;
#endif

void UiCommandServer::parseInput(const std::string& s)
{
	cmd_ = Commands::NONE;
	if (s.size() == SINGLE_COMMAND_SIZE)
	{
		switch (s.c_str()[0])
		{
		case 'r': cmd_ = Commands::CLOSE_APP; break;
		case 'g': cmd_ = Commands::GET_APPS; break;
		default: cmd_ = Commands::NONE; break;
		}
	}
	else if (s.substr(0, 2) == "k ")
	{
		std::string param = s.substr(2, s.size());
		if (param.find_first_not_of("0123456789") == std::string::npos) // if int
		{
			cmd_ = Commands::KILL_COMMAND;
			param_ = param;
		}
	}
}

void UiCommandServer::processCmd(ServerTcp& srv)
{
	switch (cmd_)
	{
	case Commands::CLOSE_APP:
	{      
		std::cout << "closing \n";
		srv.stopServer();
	}break;
	case Commands::KILL_COMMAND:
	{
   		std::cout << "killing \n";
		if(killApp(param_))
			srv.setSendMsg("successfully shutdown \n");
		else
			srv.setSendMsg("shutdown failed \n");
		srv.send();
	}break;
	case Commands::GET_APPS:
	{
		std::cout << "getting app processes \n";
		srv.setSendMsg(PrintPocesses().str());
		srv.send();
	}break;
	case Commands::NONE:
	{
		std::cout << "bad command \n";
	}break;
	default: break;
	}
}
