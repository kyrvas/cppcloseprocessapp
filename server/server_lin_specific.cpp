#if defined (_WIN32) || defined (__CYGWIN__)
#else
#include "server_lin_specific.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include "Logger.h"

namespace LinServer
{
	Logger logger;
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[256];
    struct sockaddr_in serv_addr, cli_addr;
    int n;

    int initServerFunctions(std::string ServerPort) 
    {
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0) 
		{
//			fprintf(stderr,"ERROR opening socket\n");
			logger.log("ERROR opening socket");        
			return SERVER_ERROR;
		}
		bzero((char *) &serv_addr, sizeof(serv_addr));
		portno = atoi(ServerPort.c_str());
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = INADDR_ANY;
		serv_addr.sin_port = htons(portno);
		if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
		{
//			fprintf(stderr,"ERROR on binding\n");
			logger.log("ERROR on binding");        
			return SERVER_ERROR;
		}
		listen(sockfd,5);
		return SERVER_OK;
    }

    int listenForClient()
    {
		clilen = sizeof(cli_addr);
		newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
		if (newsockfd < 0)
		{ 
//			fprintf(stderr,"ERROR on accept \n");
			logger.log("ERROR on accept");        
			return SERVER_ERROR;
		}
		return SERVER_OK;
    }

    int sendToClient(const char* sendbuf, int len) 
    {
		n = write(newsockfd,sendbuf,len);
		if (n < 0) 
		{ 
//			fprintf(stderr,"ERROR writing to socket\n");
			logger.log("ERROR writing to socket");        
			return SERVER_ERROR;
		}
		shutdown(newsockfd, SHUT_WR);
		close(newsockfd);
		return SERVER_OK;
    }

    int receiveFromClient(std::string& s) 
    {
		bzero(buffer,256);
		do 
		{
			n = read(newsockfd,buffer,255);
            if (n > 0) 
			{
				s.insert(s.size(),buffer, n);
//                printf("Bytes received: %d\n", n);
				logger.log("Bytes received:", n);        
			}
            else if (n == 0)
//                printf("Connection closing...\n");
				logger.log("Connection closing...");        
            else 
			{
//                printf("recv failed \n");
				logger.log("recv failed");        
                return SERVER_ERROR;
            }
        } while (n > 0);
        return SERVER_OK;
    }

    int shutDownServer() 
    {
		close(newsockfd);
		close(sockfd);
        return SERVER_OK;
    }
}
#endif
