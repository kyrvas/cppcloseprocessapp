#include "ClientTcp.h"

#if defined( __CYGWIN__) || defined(_WIN32)
#include "client_win_specific.h"
using namespace WinClient;
#else
#include "client_lin_specific.h"
using namespace LinClient;	
#endif

void ClientTcp::initClient(std::string ServerName, std::string ServerPort)
{
	serverName_ = ServerName;
	serverPort_ = ServerPort;
	clientOk_ = true;
	if( initClientFunctions(serverName_.c_str(), serverPort_.c_str()) != CLIENT_OK)
	{
		clientOk_ = false;
	}
}

void ClientTcp::initClient()
{
	clientOk_ = true;
	if( initClientFunctions(serverName_.c_str(), serverPort_.c_str()) != CLIENT_OK)
	{
		clientOk_ = false;
	}
}

ClientTcp::ClientTcp(std::string ServerName, std::string ServerPort) :serverName_(ServerName), serverPort_(ServerPort), clientOk_(true)
{
	if( initClientFunctions(serverName_.c_str(), serverPort_.c_str()) != CLIENT_OK)
	{
		clientOk_ = false;
	}
}

void ClientTcp::connect()
{
	received_.clear();
	if ( clientOk_ && (connectToServer() != CLIENT_OK) )
	{
		clientOk_ = false;
	}
}

void ClientTcp::send(std::string s) 
{
	if ( clientOk_ && (sendToServer(s.c_str(), s.size()) != CLIENT_OK) )
	{
		clientOk_ = false;
	}
}

void ClientTcp::send()
{
	if ( clientOk_ && (sendToServer(sended_.c_str(), sended_.size()) != CLIENT_OK) )
	{
		clientOk_ = false;
	}
}
void ClientTcp::receive()
{
	if ( clientOk_ && (receiveFromServer(received_) != CLIENT_OK) )
	{
		clientOk_ = false;
	}
}
std::string ClientTcp::getReceivedMsg()
{
	return received_;
}
void ClientTcp::setSendMsg(std::string send)
{
	sended_ = send;
}

std::string ClientTcp::getSendMsg()
{
	return sended_;
}
std::string ClientTcp::getServerName()
{
	return serverName_;
}
void ClientTcp::setServerName(std::string s)
{
	serverName_ = s;
}

std::string ClientTcp::getServerPort()
{
	return serverPort_;
}

void ClientTcp::setServerPort(std::string s)
{
	serverPort_ = s;
}

ClientTcp::~ClientTcp()
{
	shutDownClient();
};

bool ClientTcp::isOk()
{
	return clientOk_;
};

void ClientTcp::closeClient()
{
	shutDownClient();
	clientOk_ = false;
}