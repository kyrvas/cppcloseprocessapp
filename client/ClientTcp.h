#ifndef CLIENTC_H
#define CLIENTC_H

#include <string>

class ClientTcp {
public:
	ClientTcp(std::string ServerName, std::string ServerPort);

	~ClientTcp();
	std::string getServerName();
	std::string getServerPort();

private:
	std::string getReceivedMsg();
	std::string getSendMsg();
	void setSendMsg(std::string send);
	void connect();
	void send(std::string s);
	void send();
	void receive();
	void setServerName(std::string s);
	void setServerPort(std::string s);
	bool isOk();
	void initClient(std::string ServerName, std::string ServerPort);
	void initClient();
	void closeClient();
    friend class UiCommandClient;

	bool clientOk_;
	std::string serverName_;
	std::string serverPort_;
	std::string received_;
	std::string sended_;
};

#endif // !CLIENTC_H