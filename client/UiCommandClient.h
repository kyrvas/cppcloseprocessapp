#ifndef UICOMMANDCLIENT_H
#define UICOMMANDCLIENT_H

#include <string>
class ClientTcp;
class UiCommandClient
{
public:
	enum class Commands { NONE, CLOSE_APP_REMOTE, CLOSE_APP_LOCAL, KILL_REMOTE_COMMAND, GET_REMOTE_APPS, HELP_MESSAGE, CHANGE_PORT, CHANGE_SERVER };
	void parseInput(const std::string& s);
	void processCmd(ClientTcp& clnt);
	void stopUI();
	bool isRunning();
	UiCommandClient();
	~UiCommandClient() = default;
private:
	Commands cmd_;
	std::string param_;
	bool run_;
};

#endif // !UICOMMAND_H

