#if defined( _WIN32) || defined(__CYGWIN__)
#else
#include "client_lin_specific.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

namespace LinClient
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];	

    int initClientFunctions(std::string  ServerName, std::string  ServerPort) 
    {
        portno = atoi(ServerPort.c_str());
    	server = gethostbyname(ServerName.c_str());
    	if (server == NULL)
       	{
        	fprintf(stderr,"ERROR, no such host\n");
            return CLIENT_ERROR;
        }
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *)server->h_addr, 
        (char *)&serv_addr.sin_addr.s_addr,
        server->h_length);
        serv_addr.sin_port = htons(portno);
	    return CLIENT_OK;
    }
 
    int connectToServer() 
    {
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
                fprintf(stderr,"ERROR opening socket\n");
	    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	    {
		    fprintf(stderr,"ERROR connecting\n");
		    return CLIENT_ERROR;
	    }
    	return CLIENT_OK;
    }

     int sendToServer(const char* sendbuf, int len) 
     {
    	n = write(sockfd,sendbuf,len);
	    shutdown(sockfd, SHUT_WR);
        return CLIENT_OK;
     }

    int receiveFromServer(std::string &s) 
    { 
 	    do 
        {
            n = read(sockfd,buffer,255);;
            if (n > 0) 
            {
                s.insert(s.size(),buffer, n);
                printf("Bytes received: %d\n",n);
            }
            else if (n == 0)
                printf("Connection closed\n");
            else
            {
                fprintf(stderr,"ERROR reading from socket\n");
                return CLIENT_ERROR;
            }
        } while (n > 0);
        return CLIENT_OK;
    }

    int shutDownClient() 
    {
	    close(sockfd);
        return CLIENT_OK;
    }
}
#endif