#ifndef CLIENT_LIN_SPECIFIC_H
#define CLIENT_LIN_SPECIFIC_H

#if defined(_WIN32) || defined(__CYGWIN__)
#else
#include <string>

#define CLIENT_OK 0
#define CLIENT_ERROR 1

namespace LinClient
{
	int initClientFunctions(std::string ServerName, std::string ServerPort);
	int connectToServer();
	int sendToServer(const char* sendbuf, int len);
	int receiveFromServer(std::string& s);
	int shutDownClient();
}
#endif
#endif