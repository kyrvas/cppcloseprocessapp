#if defined( _WIN32) || defined(__CYGWIN__)

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512

#include "client_win_specific.h"
namespace WinClient
{
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo* result = NULL,
        * ptr = NULL,
        hints;

    char recvbuf[DEFAULT_BUFLEN];
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;
   
     int initClientFunctions(std::string ServerName, std::string ServerPort) 
     {
        iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (iResult != 0) {
            printf("WSAStartup failed with error: %d\n", iResult);
            return CLIENT_ERROR;
        }

        ZeroMemory(&hints, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;

        iResult = getaddrinfo(ServerName.c_str(), ServerPort.c_str(), &hints, &result);
        if (iResult != 0) {
            printf("getaddrinfo failed with error: %d\n", iResult);
            WSACleanup();
            return CLIENT_ERROR;
        }
        return CLIENT_OK;
    }
 
     int connectToServer() 
     {
         for (ptr = result; ptr != NULL; ptr = ptr->ai_next) 
         {
             ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                 ptr->ai_protocol);
             if (ConnectSocket == INVALID_SOCKET) 
             {
                 printf("socket failed with error: %ld\n", WSAGetLastError());
                 WSACleanup();
                 return CLIENT_ERROR;
             }

             iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
             if (iResult == SOCKET_ERROR) {
                 closesocket(ConnectSocket);
                 ConnectSocket = INVALID_SOCKET;
                 continue;
             }
             break;
         }

         if (ConnectSocket == INVALID_SOCKET) 
         {
             printf("Unable to connect to server!\n");
             WSACleanup();
             return CLIENT_ERROR;
         }
         return CLIENT_OK;
     }

     int sendToServer(const char* sendbuf, int len) 
     {
        iResult = send(ConnectSocket, sendbuf, len, 0);
        if (iResult == SOCKET_ERROR) {
            printf("send failed with error: %d\n", WSAGetLastError());
            closesocket(ConnectSocket);
            WSACleanup();
            return CLIENT_ERROR;
        }
        printf("Bytes Sent: %ld\n", iResult);
        shutdown(ConnectSocket, SD_SEND);
        return CLIENT_OK;
    }

    int receiveFromServer(std::string &s) 
    {
        do 
        {
            iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
            if (iResult > 0) 
            {
                s.insert(s.size(), recvbuf, iResult); 
                printf("Bytes received: %d\n", iResult);
            }
            else if (iResult == 0)
                printf("Connection closed\n");
            else
            {
                printf("recv failed with error: %d\n", WSAGetLastError());
                return CLIENT_ERROR;
            }

        } while (iResult > 0);
        return CLIENT_OK;
    }

    int shutDownClient() 
    {
        freeaddrinfo(result);
        shutdown(ConnectSocket, SD_BOTH);
        closesocket(ConnectSocket);
        WSACleanup();
        return CLIENT_OK;
    }
}
#endif