#ifndef CLIENT_WIN_SPECIFIC_H
#define CLIENT_WIN_SPECIFIC_H

#if defined(_WIN32) || defined(__CYGWIN__)
#include <string>

#define CLIENT_OK 0
#define CLIENT_ERROR 1

namespace WinClient
{
	int initClientFunctions(std::string ServerName, std::string ServerPort);
	int connectToServer();
	int sendToServer(const char* sendbuf, int len);
	int receiveFromServer(std::string& s);
	int shutDownClient();
};
#endif
#endif