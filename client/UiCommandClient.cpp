#include "UiCommandClient.h"
#include "ClientTcp.h"
#include <iostream>

UiCommandClient::UiCommandClient():run_(true)
{
	
}
void UiCommandClient::parseInput(const std::string& s)
{
	cmd_ = Commands::NONE;
	if (s.size() == 1)
	{
		switch (s.c_str()[0])
		{
		case 'r': cmd_ = Commands::CLOSE_APP_REMOTE; break;
		case 'q': cmd_ = Commands::CLOSE_APP_LOCAL; break;
		case 'g': cmd_ = Commands::GET_REMOTE_APPS; break;
		case 'h': cmd_ = Commands::HELP_MESSAGE; break;
		default: cmd_ = Commands::NONE; break;
		}
	}
	else 
	{
		std::string command = s.substr(0, 2); 
		std::string param = s.substr(2, s.size());

		if ( command == "k ")
		{	
			if (param.find_first_not_of("0123456789") == std::string::npos) // if int
			{
				cmd_ = Commands::KILL_REMOTE_COMMAND;
				param_ = param;
			}
		}
		if ( command == "p ")
		{	
			if (param.find_first_not_of("0123456789") == std::string::npos) // if int
			{
				cmd_ = Commands::CHANGE_PORT;
				param_ = param;
			}
		}
		if ( command == "s ")
		{	
			cmd_ = Commands::CHANGE_SERVER;
			param_ = param;
		}
		
	}
}
void UiCommandClient::stopUI()
{	
	run_ = false;
}
bool UiCommandClient::isRunning()
{
	return run_;
}

void UiCommandClient::processCmd(ClientTcp& clnt)
{
	switch (cmd_)
	{
	case Commands::CLOSE_APP_REMOTE:
	{
		if (!clnt.isOk())
		{
			clnt.initClient();
		};
		if (clnt.isOk())
		{
			clnt.connect();
			clnt.setSendMsg("r");
			clnt.send();
		}
	}break;
	case Commands::CLOSE_APP_LOCAL:
	{
		stopUI();
	}break;
	case Commands::KILL_REMOTE_COMMAND:
	{
		if (!clnt.isOk())
		{
			clnt.initClient();
		};
		if (clnt.isOk())
		{
			clnt.connect();
			clnt.setSendMsg("k " + param_);
			clnt.send();
			clnt.receive();
			std::cout << clnt.getReceivedMsg() << "\n";
		}
	}break;
	case Commands::GET_REMOTE_APPS:
	{
		if (!clnt.isOk())
		{
			clnt.initClient();
		};
		if (clnt.isOk())
		{
			clnt.connect();
			clnt.setSendMsg("g");
			clnt.send();
			clnt.receive();
			std::cout << clnt.getReceivedMsg() << "\n";
		}
	}break;
	case Commands::CHANGE_PORT:
	{
	//	clnt.closeClient();
		clnt.setServerPort(param_);
		clnt.initClient();
		if (clnt.isOk())
		{
			std::cout << "ServerPort successfully changed to " << clnt.getServerPort() << "\n";
		}
	}break;
	case Commands::CHANGE_SERVER:
	{
	//	clnt.closeClient();
		clnt.setServerName(param_);
		clnt.initClient();
		if (clnt.isOk())
		{
			std::cout << "ServerName successfully changed to " << clnt.getServerName() << ":"<< clnt.getServerPort() <<"\n";
		}else
		{
			std::cout << "unknown server \n";	
		}
	}break;
	case Commands::HELP_MESSAGE:
	{
		std::cout
			<< "use: client [ServerName] [ServerPort] ## now connects to " << 
			"ServerName = "<< clnt.getServerName() <<"\nServerPort = "<<clnt.getServerPort() << "\n\n"
			<< "type 'q' to exit \n"
			<< "type 'r' to close remote \n"
			<< "type 'g' to get remote app processes \n"
			<< "type 'k [PID]' to kill remote app with pid = PID \n"
			<< "type 'p [ServerPort]' to change port to connect \n"
			<< "type 's [ServerName]' to change host to connect \n"
			<< "type 'h' to see this message \n";
	}break;
	case Commands::NONE:
	{
		std::cout << "bad command \n";
	}break;
	default: break;
	}
	
}
