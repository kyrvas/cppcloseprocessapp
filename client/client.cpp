#include <iostream>
#include <string>
#include "ClientTcp.h"
#include "UiCommandClient.h"

int main(int argc, char** argv)
{
	std::string ServerName("127.0.0.1");
	std::string ServerPort("27015");
	if(argc >= 2)
	{
		ServerName.assign(argv[1]);
	}
	
	if(argc >= 3)
	{
		ServerPort.assign(argv[2]);
    }

	std::string input;
	UiCommandClient cmd;
	ClientTcp client(ServerName, ServerPort);
	
	cmd.parseInput("h");
	cmd.processCmd(client);
	while (cmd.isRunning()) 
	{
		std::getline(std::cin, input);
		cmd.parseInput(input);
		cmd.processCmd(client);
	}
    return 0;
}
