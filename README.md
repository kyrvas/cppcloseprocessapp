# cppCloseProcessApp

Client/Server application to close processes on remote HOST.
- client gets processes list and sends them by server request.
- server will request to close one of the processes on client's host.
- client has to reply with success or failure.

todo:
logger
multiply sockets
threads